#!/usr/bin/env python

"""
   Wrapper generator, invokes cffi and the system c compiler
   to generate c interface wrapper of tesseract-ocr"""

from cffi import FFI
import sys

ffi = FFI()

tesseract_version = 304

if sys.platform == "win32":
    usize_gt_32 = sys.maxsize > 2**32
    libs = ["libtesseract%s" % tesseract_version]
    inc_dirs = ['include']
    if usize_gt_32:
        lib_dirs = ["lib/x64"]
    else:
        lib_dirs = ["lib/x86"]
else:
    libs = ["z", "stdc++", "tesseract"]
    lib_dirs = []
    inc_dirs = ['include']

ffi.set_source("_tesseract",
               """
                #include "tesseract/capi.h"
                #include "tesseract/platform.h"
                #include <stdio.h>
               """,
               include_dirs=inc_dirs,
               library_dirs=lib_dirs,
               libraries=libs,
               source_extension=".c")

ffi.cdef("""
typedef struct TessResultRenderer TessResultRenderer;
typedef struct TessTextRenderer TessTextRenderer;
typedef struct TessHOcrRenderer TessHOcrRenderer;
typedef struct TessPDFRenderer TessPDFRenderer;
typedef struct TessUnlvRenderer TessUnlvRenderer;
typedef struct TessBoxTextRenderer TessBoxTextRenderer;
typedef struct TessBaseAPI TessBaseAPI;
typedef struct TessPageIterator TessPageIterator;
typedef struct TessResultIterator TessResultIterator;
typedef struct TessMutableIterator TessMutableIterator;
typedef struct TessChoiceIterator TessChoiceIterator;
typedef enum TessOcrEngineMode {
  OEM_TESSERACT_ONLY,
  OEM_CUBE_ONLY,
  OEM_TESSERACT_CUBE_COMBINED,
  OEM_DEFAULT
} TessOcrEngineMode;
typedef enum TessPageSegMode {
  PSM_OSD_ONLY,
  PSM_AUTO_OSD,
  PSM_AUTO_ONLY,
  PSM_AUTO,
  PSM_SINGLE_COLUMN,
  PSM_SINGLE_BLOCK_VERT_TEXT,
  PSM_SINGLE_BLOCK,
  PSM_SINGLE_LINE,
  PSM_SINGLE_WORD,
  PSM_CIRCLE_WORD,
  PSM_SINGLE_CHAR,
  PSM_SPARSE_TEXT,
  PSM_SPARSE_TEXT_OSD,
  PSM_COUNT
} TessPageSegMode;
typedef enum TessPageIteratorLevel {
  RIL_BLOCK,
  RIL_PARA,
  RIL_TEXTLINE,
  RIL_WORD,
  RIL_SYMBOL
} TessPageIteratorLevel;
typedef enum TessPolyBlockType {
  PT_UNKNOWN,
  PT_FLOWING_TEXT,
  PT_HEADING_TEXT,
  PT_PULLOUT_TEXT,
  PT_EQUATION,
  PT_INLINE_EQUATION,
  PT_TABLE,
  PT_VERTICAL_TEXT,
  PT_CAPTION_TEXT,
  PT_FLOWING_IMAGE,
  PT_HEADING_IMAGE,
  PT_PULLOUT_IMAGE,
  PT_HORZ_LINE,
  PT_VERT_LINE,
  PT_NOISE,
  PT_COUNT
} TessPolyBlockType;
typedef enum TessOrientation {
  ORIENTATION_PAGE_UP,
  ORIENTATION_PAGE_RIGHT,
  ORIENTATION_PAGE_DOWN,
  ORIENTATION_PAGE_LEFT
} TessOrientation;
typedef enum TessWritingDirection {
  WRITING_DIRECTION_LEFT_TO_RIGHT,
  WRITING_DIRECTION_RIGHT_TO_LEFT,
  WRITING_DIRECTION_TOP_TO_BOTTOM
} TessWritingDirection;
typedef enum TessTextlineOrder {
  TEXTLINE_ORDER_LEFT_TO_RIGHT,
  TEXTLINE_ORDER_RIGHT_TO_LEFT,
  TEXTLINE_ORDER_TOP_TO_BOTTOM
} TessTextlineOrder;
typedef struct ETEXT_DESC ETEXT_DESC;

struct Pix;
struct Boxa;
struct Pixa;

/* General free functions */

const char *TessVersion();
void TessDeleteText(char *text);
void TessDeleteTextArray(char **arr);
void TessDeleteIntArray(int *arr);

/* Renderer API */
TessResultRenderer *TessTextRendererCreate(const char *outputbase);
TessResultRenderer *TessHOcrRendererCreate(const char *outputbase);
TessResultRenderer *TessHOcrRendererCreate2(const char *outputbase,
                                            int font_info);
TessResultRenderer *TessPDFRendererCreate(const char *outputbase,
                                          const char *datadir);
TessResultRenderer *TessUnlvRendererCreate(const char *outputbase);
TessResultRenderer *TessBoxTextRendererCreate(const char *outputbase);

void TessDeleteResultRenderer(TessResultRenderer *renderer);
void TessResultRendererInsert(TessResultRenderer *renderer,
                              TessResultRenderer *next);
TessResultRenderer *TessResultRendererNext(TessResultRenderer *renderer);
int TessResultRendererBeginDocument(TessResultRenderer *renderer,
                                    const char *title);
int TessResultRendererAddImage(TessResultRenderer *renderer, TessBaseAPI *api);
int TessResultRendererEndDocument(TessResultRenderer *renderer);

const char *TessResultRendererExtention(TessResultRenderer *renderer);
const char *TessResultRendererTitle(TessResultRenderer *renderer);
int TessResultRendererImageNum(TessResultRenderer *renderer);

/* Base API */

TessBaseAPI *TessBaseAPICreate();
void TessBaseAPIDelete(TessBaseAPI *handle);

size_t TessBaseAPIGetOpenCLDevice(TessBaseAPI *handle, void **device);

void TessBaseAPISetInputName(TessBaseAPI *handle, const char *name);
const char *TessBaseAPIGetInputName(TessBaseAPI *handle);

void TessBaseAPISetInputImage(TessBaseAPI *handle, struct Pix *pix);
struct Pix *TessBaseAPIGetInputImage(TessBaseAPI *handle);

int TessBaseAPIGetSourceYResolution(TessBaseAPI *handle);
const char *TessBaseAPIGetDatapath(TessBaseAPI *handle);

void TessBaseAPISetOutputName(TessBaseAPI *handle, const char *name);

int TessBaseAPISetVariable(TessBaseAPI *handle, const char *name,
                           const char *value);
int TessBaseAPISetDebugVariable(TessBaseAPI *handle, const char *name,
                                const char *value);

int TessBaseAPIGetIntVariable(const TessBaseAPI *handle, const char *name,
                              int *value);
int TessBaseAPIGetBoolVariable(const TessBaseAPI *handle, const char *name,
                               int *value);
int TessBaseAPIGetDoubleVariable(const TessBaseAPI *handle, const char *name,
                                 double *value);
const char *TessBaseAPIGetStringVariable(const TessBaseAPI *handle,
                                         const char *name);

void TessBaseAPIPrintVariables(const TessBaseAPI *handle, FILE *fp);
int TessBaseAPIPrintVariablesToFile(const TessBaseAPI *handle,
                                    const char *filename);

int TessBaseAPIInit1(TessBaseAPI *handle, const char *datapath,
                     const char *language, TessOcrEngineMode oem,
                     char **configs, int configs_size);
int TessBaseAPIInit2(TessBaseAPI *handle, const char *datapath,
                     const char *language, TessOcrEngineMode oem);
int TessBaseAPIInit3(TessBaseAPI *handle, const char *datapath,
                     const char *language);

int TessBaseAPIInit4(TessBaseAPI *handle, const char *datapath,
                     const char *language, TessOcrEngineMode mode,
                     char **configs, int configs_size, char **vars_vec,
                     char **vars_values, size_t vars_vec_size,
                     int set_only_non_debug_params);

const char *TessBaseAPIGetInitLanguagesAsString(const TessBaseAPI *handle);
char **TessBaseAPIGetLoadedLanguagesAsVector(const TessBaseAPI *handle);
char **TessBaseAPIGetAvailableLanguagesAsVector(const TessBaseAPI *handle);

int TessBaseAPIInitLangMod(TessBaseAPI *handle, const char *datapath,
                           const char *language);
void TessBaseAPIInitForAnalysePage(TessBaseAPI *handle);

void TessBaseAPIReadConfigFile(TessBaseAPI *handle, const char *filename);
void TessBaseAPIReadDebugConfigFile(TessBaseAPI *handle, const char *filename);

void TessBaseAPISetPageSegMode(TessBaseAPI *handle, TessPageSegMode mode);
TessPageSegMode TessBaseAPIGetPageSegMode(const TessBaseAPI *handle);

char *TessBaseAPIRect(TessBaseAPI *handle, const unsigned char *imagedata,
                      int bytes_per_pixel, int bytes_per_line, int left,
                      int top, int width, int height);

void TessBaseAPIClearAdaptiveClassifier(TessBaseAPI *handle);

void TessBaseAPISetImage(TessBaseAPI *handle, const unsigned char *imagedata,
                         int width, int height, int bytes_per_pixel,
                         int bytes_per_line);
void TessBaseAPISetImage2(TessBaseAPI *handle, struct Pix *pix);

void TessBaseAPISetSourceResolution(TessBaseAPI *handle, int ppi);

void TessBaseAPISetRectangle(TessBaseAPI *handle, int left, int top, int width,
                             int height);

struct Pix *TessBaseAPIGetThresholdedImage(TessBaseAPI *handle);
struct Boxa *TessBaseAPIGetRegions(TessBaseAPI *handle, struct Pixa **pixa);
struct Boxa *TessBaseAPIGetTextlines(TessBaseAPI *handle, struct Pixa **pixa,
                                     int **blockids);
struct Boxa *TessBaseAPIGetTextlines1(TessBaseAPI *handle, const int raw_image,
                                      const int raw_padding, struct Pixa **pixa,
                                      int **blockids, int **paraids);
struct Boxa *TessBaseAPIGetStrips(TessBaseAPI *handle, struct Pixa **pixa,
                                  int **blockids);
struct Boxa *TessBaseAPIGetWords(TessBaseAPI *handle, struct Pixa **pixa);
struct Boxa *TessBaseAPIGetConnectedComponents(TessBaseAPI *handle,
                                               struct Pixa **cc);
struct Boxa *TessBaseAPIGetComponentImages(TessBaseAPI *handle,
                                           const TessPageIteratorLevel level,
                                           const int text_only,
                                           struct Pixa **pixa, int **blockids);
struct Boxa *TessBaseAPIGetComponentImages1(
    TessBaseAPI *handle, const TessPageIteratorLevel level, const int text_only,
    const int raw_image, const int raw_padding, struct Pixa **pixa,
    int **blockids, int **paraids);

int TessBaseAPIGetThresholdedImageScaleFactor(const TessBaseAPI *handle);

void TessBaseAPIDumpPGM(TessBaseAPI *handle, const char *filename);

TessPageIterator *TessBaseAPIAnalyseLayout(TessBaseAPI *handle);

int TessBaseAPIRecognize(TessBaseAPI *handle, ETEXT_DESC *monitor);
int TessBaseAPIRecognizeForChopTest(TessBaseAPI *handle, ETEXT_DESC *monitor);
int TessBaseAPIProcessPages(TessBaseAPI *handle, const char *filename,
                            const char *retry_config, int timeout_millisec,
                            TessResultRenderer *renderer);
int TessBaseAPIProcessPage(TessBaseAPI *handle, struct Pix *pix, int page_index,
                           const char *filename, const char *retry_config,
                           int timeout_millisec, TessResultRenderer *renderer);

TessResultIterator *TessBaseAPIGetIterator(TessBaseAPI *handle);
TessMutableIterator *TessBaseAPIGetMutableIterator(TessBaseAPI *handle);

char *TessBaseAPIGetUTF8Text(TessBaseAPI *handle);
char *TessBaseAPIGetHOCRText(TessBaseAPI *handle, int page_number);
char *TessBaseAPIGetBoxText(TessBaseAPI *handle, int page_number);
char *TessBaseAPIGetUNLVText(TessBaseAPI *handle);
int TessBaseAPIMeanTextConf(TessBaseAPI *handle);
int *TessBaseAPIAllWordConfidences(TessBaseAPI *handle);
int TessBaseAPIAdaptToWordStr(TessBaseAPI *handle, TessPageSegMode mode,
                              const char *wordstr);

void TessBaseAPIClear(TessBaseAPI *handle);
void TessBaseAPIEnd(TessBaseAPI *handle);

int TessBaseAPIIsValidWord(TessBaseAPI *handle, const char *word);
int TessBaseAPIGetTextDirection(TessBaseAPI *handle, int *out_offset,
                                float *out_slope);

const char *TessBaseAPIGetUnichar(TessBaseAPI *handle, int unichar_id);

void TessBaseAPISetMinOrientationMargin(TessBaseAPI *handle, double margin);

/* Page iterator */

void TessPageIteratorDelete(TessPageIterator *handle);
TessPageIterator *TessPageIteratorCopy(const TessPageIterator *handle);

void TessPageIteratorBegin(TessPageIterator *handle);
int TessPageIteratorNext(TessPageIterator *handle, TessPageIteratorLevel level);
int TessPageIteratorIsAtBeginningOf(const TessPageIterator *handle,
                                    TessPageIteratorLevel level);
int TessPageIteratorIsAtFinalElement(const TessPageIterator *handle,
                                     TessPageIteratorLevel level,
                                     TessPageIteratorLevel element);

int TessPageIteratorBoundingBox(const TessPageIterator *handle,
                                TessPageIteratorLevel level, int *left,
                                int *top, int *right, int *bottom);
TessPolyBlockType TessPageIteratorBlockType(const TessPageIterator *handle);

struct Pix *TessPageIteratorGetBinaryImage(const TessPageIterator *handle,
                                           TessPageIteratorLevel level);
struct Pix *TessPageIteratorGetImage(const TessPageIterator *handle,
                                     TessPageIteratorLevel level, int padding,
                                     struct Pix *original_image, int *left,
                                     int *top);

int TessPageIteratorBaseline(const TessPageIterator *handle,
                             TessPageIteratorLevel level, int *x1, int *y1,
                             int *x2, int *y2);

void TessPageIteratorOrientation(TessPageIterator *handle,
                                 TessOrientation *orientation,
                                 TessWritingDirection *writing_direction,
                                 TessTextlineOrder *textline_order,
                                 float *deskew_angle);

/* Result iterator */

void TessResultIteratorDelete(TessResultIterator *handle);
TessResultIterator *TessResultIteratorCopy(const TessResultIterator *handle);
TessPageIterator *TessResultIteratorGetPageIterator(TessResultIterator *handle);
const TessPageIterator *
TessResultIteratorGetPageIteratorConst(const TessResultIterator *handle);
const TessChoiceIterator *
TessResultIteratorGetChoiceIterator(const TessResultIterator *handle);

int TessResultIteratorNext(TessResultIterator *handle,
                           TessPageIteratorLevel level);
char *TessResultIteratorGetUTF8Text(const TessResultIterator *handle,
                                    TessPageIteratorLevel level);
float TessResultIteratorConfidence(const TessResultIterator *handle,
                                   TessPageIteratorLevel level);
const char *
TessResultIteratorWordRecognitionLanguage(const TessResultIterator *handle);
const char *TessResultIteratorWordFontAttributes(
    const TessResultIterator *handle, int *is_bold, int *is_italic,
    int *is_underlined, int *is_monospace, int *is_serif, int *is_smallcaps,
    int *pointsize, int *font_id);

int TessResultIteratorWordIsFromDictionary(const TessResultIterator *handle);
int TessResultIteratorWordIsNumeric(const TessResultIterator *handle);
int TessResultIteratorSymbolIsSuperscript(const TessResultIterator *handle);
int TessResultIteratorSymbolIsSubscript(const TessResultIterator *handle);
int TessResultIteratorSymbolIsDropcap(const TessResultIterator *handle);

void TessChoiceIteratorDelete(TessChoiceIterator *handle);
int TessChoiceIteratorNext(TessChoiceIterator *handle);
const char *TessChoiceIteratorGetUTF8Text(const TessChoiceIterator *handle);
float TessChoiceIteratorConfidence(const TessChoiceIterator *handle);
""")

ffi.compile()
