#Intro

tesseract_cffi uses the cffi bindings generator to wrap tesseract-ocr's C api.

#Install:

      pip install -r requirements.txt
      python setup.py {develop,install}

  clone the repository and _run "python setup.py install" (or "python setup.py
  develop" to avoid reinstalling every time you pull changes)

  It's suggested you use python setup.py develop since they project will be
  undergoing _very_ active development as more features are required.

#Build Wheel:

    python setup.py bdist_wheel

##Windows
  tesseract304.dll from the appropriate lib/{x64,x86} folder needs
  to be placed somewhere on python's path. If you've used setup.py install then
  they should be dropped in your python installations site-packages directory.
  If setup.py develop was used then copying them up to the top level of the
  repository will do.

##Linux
  Ensure tesseract version 3.04 is installed somewhere on the system library
  path.

#Usage:
  To use the python_tesseract compat module

      import tesseract_cffi.python_tesseract_compat as tesseract


  TODO: higher level wrapper
