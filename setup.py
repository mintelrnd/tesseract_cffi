"""setup.py that executes cffi wrapper generation"""

from setuptools import setup

setup(name="tesseract_cffi",
      version="0.2.1",
      description="cffi based wrapper for google's tesseract-ocr library",
      packages=['tesseract_cffi', 'tesseract_cffi.python_tesseract_compat'],
      package_data={'tesseract_cffi': ['build_tesseract_bindings.py']},
      setup_requires=["cffi>=1.0.0"],
      cffi_modules=["build_tesseract_bindings.py:ffi"],
      install_requires=["cffi>=1.0.0"])
