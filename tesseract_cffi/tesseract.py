"""
This module contains the utilities for integrating tesseract
with the gnpd intellisight pipeline.
"""
import os
from tesseract_cffi.python_tesseract_compat import \
    TessBasePageIterator, TessPageIteratorIsAtFinalElement, \
    TessPageIteratorIsAtBeginningOf, TessBaseChoiceIterator, TessBaseAPI, \
    OEM_DEFAULT, PSM_AUTO, RIL_SYMBOL, RIL_BLOCK, RIL_PARA, RIL_TEXTLINE, \
    RIL_WORD


LEVELS = {"word": RIL_WORD,
          "line": RIL_TEXTLINE,
          "paragraph": RIL_PARA,
          "block": RIL_BLOCK,
          "symbol": RIL_SYMBOL}


class OCREngine(object):
    """
    This class initialises tesseract, to which the recognise_text
    function can be called to run ocr on a grayscale image.
    """
    def __init__(self):
        self._api = TessBaseAPI()
        if 'TESSDATA_PREFIX' not in os.environ:
            raise EnvironmentError(
                'TESSDATA_PREFIX is not set to Tesseract language files')
        else:
            self._api.Init(os.environ["TESSDATA_PREFIX"], "eng",
                           OEM_DEFAULT)
            self._api.SetPageSegMode(PSM_AUTO)
        self._levels = LEVELS

    def __del__(self):
        self._api.End()

    def set_image(self, img_data, img_width, img_height,
                  bytes_per_pixel, bytes_per_line):
        """
        Sets the image that the OCREngine will read.

        :param: img_data: The raw data bytes in an array.
                          Can be found using img.tostring().
        :param: img_width: Integer width of image.
        :param: img_height: Integer height of image.
        :param: bytes_per_pixel: Integer number of bytes per pixel.
                                 Can be calculated using img.depth / 8.
        :param: bytes_per_line: Integer number of bytes per line.
                                Can be calculated using
                                img_width * bytes_per_pixel.
        """
        self._api.SetImage(img_data, img_width, img_height,
                           bytes_per_pixel, bytes_per_line)

    def recognise_text(self):
        """
        Returns an iterator of the detected text and a metadata object for
        each piece of text recognised.
        @param: cv_img: An grayscale image loaded with
            cv2.cv.CreateImageHeader() and cv2.cv.SetData.
        @param: level: The level of grouping text should be evaluated.
                       This can be either per word, line, paragraph, block,
                       symbol. Default: "word"
        """
        self._api.Recognize(None)
        results_iter = _OCRResultIterator(self._api.GetIterator())
        return [syms for block in results_iter for syms in block]


class _OCRResultIterator(object):
    """
    This is an iterator for the recognised text and its metadata.
    It contains the functionality to add an offset to a text's roi, which is
    useful when dealing with cropped images and you want its position in the
    original image.
    """
    def __init__(self, result_iterator):
        self._ri = result_iterator
        self._pi = TessBasePageIterator(self._ri)
        self._done = TessPageIteratorIsAtFinalElement(self._pi.handle, RIL_BLOCK, RIL_SYMBOL)

    def __iter__(self):
        return self

    def next(self):
        """
        For each iteration, the recognised text is returned along with the
        ocr meta data (see OCRMeta class).
        """
        if self._done:
            raise StopIteration()
        return self._get_symbol_rois_and_choices()

    def __next__(self):
        self.next()

    def _ri_advance(self, level):
        """
        Advances the results iterator by the specified level, and checks
        if the iterator is finished.
        """
        self._done = False if self._ri.Next(level) else True

    def _start_of_word(self):
        """
        Returns True if the page iterator is at the start of a word.
        """
        return TessPageIteratorIsAtBeginningOf(self._pi.handle, RIL_WORD)

    def _start_of_line(self):
        """
        Returns True if the page iterator is at the start of a word.
        """
        return TessPageIteratorIsAtBeginningOf(self._pi.handle, RIL_TEXTLINE)

    def _get_symbol_rois_and_choices(self):
        """
        For each recognised character this function will return itself and
        other possible choices with a confidence.

        :return: a tuple of either " " (word breaks), "\\n" (line breaks) or
                 result tuples which contain a tuple of bounding box coords,
                 and a tuple of choices and confidences.
                 [(x1, y1, x2, y2), ((char, conf), ...),
                 ... " ", ... "\\n",...]
        """
        ocr_units = []
        while not self._done:
            choices = self.get_choices(TessBaseChoiceIterator(self._ri))
            bbox = self._pi.BoundingBox(RIL_SYMBOL)
            ocr_units.append((bbox, choices))
            self._ri_advance(RIL_SYMBOL)
            if self._start_of_line():
                ocr_units.append('\n')
            elif self._start_of_word():
                ocr_units.append(' ')
        return tuple(ocr_units)

    @staticmethod
    def get_choices(choice_iter):
        """
        Extracts the possible choices for a given choice iterator.

        :return: list of choice tuples containing the character and the
                 confidence.
                 [(char, conf), ... ]
        """
        choices = []
        c_text = choice_iter.GetUTF8Text().decode("utf-8")
        c_conf = choice_iter.Confidence()
        choices.append((c_text, c_conf))
        while choice_iter.Next():
            c_text = choice_iter.GetUTF8Text().decode("utf-8")
            c_conf = choice_iter.Confidence()
            choices.append((c_text, c_conf))
        return choices
