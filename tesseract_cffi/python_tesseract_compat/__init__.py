from _tesseract import lib as _lib
from _tesseract import ffi as _ffi

lang = "eng"

OEM_TESSERACT_ONLY = _lib.OEM_TESSERACT_ONLY
OEM_CUBE_ONLY = _lib.OEM_CUBE_ONLY
OEM_TESSERACT_CUBE_COMBINED = _lib.OEM_TESSERACT_CUBE_COMBINED
OEM_DEFAULT = _lib.OEM_DEFAULT
PSM_OSD_ONLY = _lib.PSM_OSD_ONLY
PSM_AUTO_OSD = _lib.PSM_AUTO_OSD
PSM_AUTO_ONLY = _lib.PSM_AUTO_ONLY
PSM_AUTO = _lib.PSM_AUTO
PSM_SINGLE_COLUMN = _lib.PSM_SINGLE_COLUMN
PSM_SINGLE_BLOCK_VERT_TEXT = _lib.PSM_SINGLE_BLOCK_VERT_TEXT
PSM_SINGLE_BLOCK = _lib.PSM_SINGLE_BLOCK
PSM_SINGLE_LINE = _lib.PSM_SINGLE_LINE
PSM_SINGLE_WORD = _lib.PSM_SINGLE_WORD
PSM_CIRCLE_WORD = _lib.PSM_CIRCLE_WORD
PSM_SINGLE_CHAR = _lib.PSM_SINGLE_CHAR
PSM_SPARSE_TEXT = _lib.PSM_SPARSE_TEXT
PSM_SPARSE_TEXT_OSD = _lib.PSM_SPARSE_TEXT_OSD
PSM_COUNT = _lib.PSM_COUNT
RIL_BLOCK = _lib.RIL_BLOCK
RIL_PARA = _lib.RIL_PARA
RIL_TEXTLINE = _lib.RIL_TEXTLINE
RIL_WORD = _lib.RIL_WORD
RIL_SYMBOL = _lib.RIL_SYMBOL
PT_UNKNOWN = _lib.PT_UNKNOWN
PT_FLOWING_TEXT = _lib.PT_FLOWING_TEXT
PT_HEADING_TEXT = _lib.PT_HEADING_TEXT
PT_PULLOUT_TEXT = _lib.PT_PULLOUT_TEXT
PT_EQUATION = _lib.PT_EQUATION
PT_INLINE_EQUATION = _lib.PT_INLINE_EQUATION
PT_TABLE = _lib.PT_TABLE
PT_VERTICAL_TEXT = _lib.PT_VERTICAL_TEXT
PT_CAPTION_TEXT = _lib.PT_CAPTION_TEXT
PT_FLOWING_IMAGE = _lib.PT_FLOWING_IMAGE
PT_HEADING_IMAGE = _lib.PT_HEADING_IMAGE
PT_PULLOUT_IMAGE = _lib.PT_PULLOUT_IMAGE
PT_HORZ_LINE = _lib.PT_HORZ_LINE
PT_VERT_LINE = _lib.PT_VERT_LINE
PT_NOISE = _lib.PT_NOISE
PT_COUNT = _lib.PT_COUNT
ORIENTATION_PAGE_UP = _lib.ORIENTATION_PAGE_UP
ORIENTATION_PAGE_RIGHT = _lib.ORIENTATION_PAGE_RIGHT
ORIENTATION_PAGE_DOWN = _lib.ORIENTATION_PAGE_DOWN
ORIENTATION_PAGE_LEFT = _lib.ORIENTATION_PAGE_LEFT
WRITING_DIRECTION_LEFT_TO_RIGHT = _lib.WRITING_DIRECTION_LEFT_TO_RIGHT
WRITING_DIRECTION_RIGHT_TO_LEFT = _lib.WRITING_DIRECTION_RIGHT_TO_LEFT
WRITING_DIRECTION_TOP_TO_BOTTOM = _lib.WRITING_DIRECTION_TOP_TO_BOTTOM
TEXTLINE_ORDER_LEFT_TO_RIGHT = _lib.TEXTLINE_ORDER_LEFT_TO_RIGHT
TEXTLINE_ORDER_RIGHT_TO_LEFT = _lib.TEXTLINE_ORDER_RIGHT_TO_LEFT
TEXTLINE_ORDER_TOP_TO_BOTTOM = _lib.TEXTLINE_ORDER_TOP_TO_BOTTOM

class TesseractException(Exception): pass

class TessBaseAPI(object):

    def __init__(self):
        pass

    def Init(self, tess_data_prefix, lang, engine_mode):
        self.version = _lib.TessVersion()
        self._handle = _lib.TessBaseAPICreate()
        if (_lib.TessBaseAPIInit2(self._handle, tess_data_prefix, lang, OEM_DEFAULT)):
            _lib.TessBaseAPIDelete(self._handle)
            raise TesseractException("TessBaseAPIInit Failed")

    def SetPageSegMode(self, mode):
        _lib.TessBaseAPISetPageSegMode(self._handle, mode)

    def SetImage(self, img_data, img_width, img_height,
                 bytes_per_pixel, bytes_per_line):
        """
        Sets the image that will be read in by tesseract.

        :param: img_data: The raw data bytes in an array.
                          Can be found using img.tostring().
        :param: img_width: Integer width of image.
        :param: img_height: Integer height of image.
        :param: bytes_per_pixel: Integer number of bytes per pixel.
                                 Can be calculated using img.depth / 8.
        :param: bytes_per_line: Integer number of bytes per line.
                                Can be calculated using
                                img_width * bytes_per_pixel.
        """
        _lib.TessBaseAPISetImage(self._handle, img_data, img_width, img_height,
                                 bytes_per_pixel, bytes_per_line)

    def Recognize(self, monitor):
        if monitor != None:
            raise NotImplementedError
        return _lib.TessBaseAPIRecognize(self._handle, _ffi.NULL)

    def GetIterator(self):
        return TessBaseResultIterator(_lib.TessBaseAPIGetIterator(self._handle))

    def End(self):
        _lib.TessBaseAPIEnd(self._handle)


class TessBaseResultIterator(object):
    def __init__(self, handle):
        self.handle = handle

    def GetUTF8Text(self, level):
        r = _lib.TessResultIteratorGetUTF8Text(self.handle, level)
        if (r != _ffi.NULL):
            return _ffi.string(r)
        else:
            return ""

    def Confidence(self, level):
        return _lib.TessResultIteratorConfidence(self.handle, level)

    def Next(self, level):
        return _lib.TessResultIteratorNext(self.handle, level)


class TessBasePageIterator(object):
    def __init__(self, result_iterator):
        self._handle = _lib.TessResultIteratorGetPageIterator(result_iterator.handle)

    @property
    def handle(self):
        return self._handle

    def BoundingBox(self, level):
        top_left_x =_ffi.new("int *")
        top_left_y =_ffi.new("int *")
        bot_right_x =_ffi.new("int *")
        bot_right_y=_ffi.new("int *")
        r = _lib.TessPageIteratorBoundingBox(self._handle, level,
                                             top_left_x, top_left_y,
                                             bot_right_x, bot_right_y)

        if (r != _ffi.NULL):
            return top_left_x[0], top_left_y[0], \
                   bot_right_x[0], bot_right_y[0]
        else:
            return None

    def Next(self, level):
        return _lib.TessPageIteratorNext(self._handle, level)


class TessBaseChoiceIterator(object):
    def __init__(self, result_iterator):
        """
        A choice iterator requires a results iterator that is not at the end of
        its iterations.
        """
        # Check prevents SEG Fault
        if len(result_iterator.GetUTF8Text(RIL_WORD)) == 0:
            raise ValueError("There are no words in result iterator.")
        else:
            self._handle = _lib.TessResultIteratorGetChoiceIterator(result_iterator.handle)

    @property
    def handle(self):
        return self._handle

    def GetUTF8Text(self):
        r = _lib.TessChoiceIteratorGetUTF8Text(self.handle)
        if (r != _ffi.NULL):
            return _ffi.string(r)
        else:
            return ""

    def Confidence(self):
        return _lib.TessChoiceIteratorConfidence(self.handle)

    def Next(self):
        return _lib.TessChoiceIteratorNext(self.handle)

def TessPageIteratorIsAtFinalElement(handle, level, element_level):
    """
    This function will tell you if a page iterator is at the final element of a
    given level. So if you wanted to know if the page iterator is at the final
    word on a line you would have:

        TessPageIteratorIsAtFinalElement(pi_handle, RIL_TEXTLINE, RIL_WORD)

    Returning True or False.
    """
    return bool(_lib.TessPageIteratorIsAtFinalElement(handle, level, element_level))


def TessPageIteratorIsAtBeginningOf(handle, level):
    """
    Returns True if the Page Iterator is at the beginning the defined level.
    """
    return bool(_lib.TessPageIteratorIsAtBeginningOf(handle, level))
